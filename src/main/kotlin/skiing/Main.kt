package skiing

import java.awt.Point
import java.io.File
import kotlin.math.abs

fun main(args: Array<String>) {
    ProcessMap().start()
}

class ProcessMap {
    private var dim = Point(0, 0)
    private var map = arrayOf<Array<Int>>()
    private var mapChecked = arrayOf<Array<Boolean>>()
    private var longRoute = mutableListOf<Point>()

    init {
        File("map.txt").forEachLine {
            val nums = it.split(" ").map { it.toInt() }.toTypedArray()

            if (dim.x == 0) { // First line
                dim = Point(nums[0], nums[1])
            } else {
                map += arrayOf(nums)
            }
        }
        mapChecked = Array(dim.y) { Array(dim.x) { false } } // Create a matrix of [dim.x * dim.y] with 'false' values
    }

    fun start() {
        for (y in 0 until dim.y) {
            for (x in 0 until dim.x) {
                if (!mapChecked[y][x]) {
                    check(Point(x, y), mutableListOf()) // Start checking new route
                }
            }
        }
        val first = longRoute.first()
        println("(${first.x}, ${first.y})=${map[first.y][first.x]}")
        println("length: ${longRoute.size}")
        println(longRoute)
    }

    private fun check(p: Point, route: MutableList<Point>) {
        mapChecked[p.y][p.x] = true
        route.add(p)
        val pVal = map[p.y][p.x]

        if (
                (p.x == 0 || map[p.y][p.x - 1] >= pVal) &&
                (p.x == dim.x - 1 || map[p.y][p.x + 1] >= pVal) &&
                (p.y == 0 || map[p.y - 1][p.x] >= pVal) &&
                (p.y == dim.y - 1 || map[p.y + 1][p.x] >= pVal)
        ) {
            // End of this route
            if (route.size > longRoute.size) {
                longRoute = route.toMutableList() // Clone
            } else if (route.size == longRoute.size) {
                // Steepest vertical drop
                if (abs(route.first().y - route.last().y) > abs(longRoute.first().y - longRoute.last().y)) {
                    longRoute = route.toMutableList() // Clone
                }
            }
        } else {
            // To right
            if (p.x + 1 < dim.x) {
                if (map[p.y][p.x + 1] < pVal) {
                    check(Point(p.x + 1, p.y), route)
                }
            }

            // To left
            if (p.x > 0) {
                if (map[p.y][p.x - 1] < pVal) {
                    check(Point(p.x - 1, p.y), route)
                }
            }

            // To down
            if (p.y + 1 < dim.y) {
                if (map[p.y + 1][p.x] < pVal) {
                    check(Point(p.x, p.y + 1), route)
                }
            }

            // To Up
            if (p.y > 0) {
                if (map[p.y - 1][p.x] < pVal) {
                    check(Point(p.x, p.y - 1), route)
                }
            }
        }

        route.removeAt(route.size - 1)
    }
}
